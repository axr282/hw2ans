clear all
clc
diary('Qu 1,2,4')
%%Question: 1
disp('Question 1')
va= -1;
vb= -1;
vc= -1;
Qa =@(pa,pb,pc)(exp(va-pa)/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc)));
Qb =@(pa,pb,pc)(exp(vb-pb)/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc)));
Qc =@(pa,pb,pc)(exp(vc-pc)/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc)));
Q0 =@(pa,pb,pc) 1/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));
qa= Qa(1,1,1)
qb= Qb(1,1,1)
qc= Qc(1,1,1)
q0= Q0(1,1,1)
%%Question: 2
disp('Question 2')
MC = @(p) [p(1)*((exp(va-p(1))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3))))) - p(1)  + 1;
    p(2)*((exp(vb-p(2))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3))))) - p(2)  + 1;
    p(3)*((exp(vc-p(3))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3))))) - p(3)  + 1] ;
tic
MCprice1= broyden(MC, [1;1;1])
t= MC(MCprice1);
toc
tic
MCprice2= broyden(MC, [0;0;0])
t= MC(MCprice2);
toc
tic
MCprice3= broyden(MC, [0;1;2])
t= MC(MCprice3);
toc
tic
MCprice4= broyden(MC, [3;2;1])
t= MC(MCprice4);
toc
%%Question: 4
disp('Question 4')
va= -1;
vb= -1;
vc= -1;
MD = @(p) [((exp(va-p(1))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)))));
((exp(vb-p(2))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)))));
((exp(vc-p(3))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)))))];
N=0;
PM=[1,1,1;0,0,0;0,1,2;3,2,1];
for i=1:4
    tic
    pt= PM(i,:);
    p=pt';
    tol=1;
while tol>1e-10
    if N>1000
        break
    end
    pn= 1./(1-MD(p));
    tol= abs(pn-p);
    p=pn;
    N= N+1;
end
p
toc
i=i+1;
end

   
    
